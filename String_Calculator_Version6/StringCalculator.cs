﻿using System.Collections.Generic;
using System;
namespace String_Calculator_Version6
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            var sum = 0;
            if (string.IsNullOrEmpty(numbers))
            {
                return sum;
            }
            var convertedNumbers = GetNumbers(numbers);
             convertedNumbers.ForEach(l => sum += l);

            return sum;
        }

        public List<int> GetNumbers(string numbers)
        {
            List<int> convertedNumbers = new List<int>();
            string[] numberList = numbers.Split(',','\n');
            foreach(string number in numberList)
            {
                convertedNumbers.Add(int.Parse(number));
            }
            return convertedNumbers;
        }

    }
}
