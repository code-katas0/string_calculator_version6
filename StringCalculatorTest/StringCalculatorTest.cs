﻿using System;
using NUnit.Framework;
using String_Calculator_Version6;

namespace StringCalculatorTest
{
  
    public class StringCalculatorTest
    {
        private StringCalculator _calculator;

        [SetUp]
        public void Setup()
        {
            _calculator = new StringCalculator();
        }

        [Test]
        public void GivenEmptyString_WhenAdding_ThenReturnZero()
        {
            //Arrange
            var expected = 0;

            //Act
            var actual = _calculator.Add("");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenOneNumber_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 1;

            //Act
            var actual = _calculator.Add("1");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenTwoNumbers_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1,2");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenUnlimitedNumbers_WhenAdding_ThenReturnSum() 
        {
            //Arrange
            var expected = 10;

            //Act
            var actual = _calculator.Add("1,2,2,4,1");

            //Assert
            Assert.AreEqual(expected,actual);
        }

        [Test]
        public void GivenNewLineAsDelimiter_WhenAdding_ThenReturnSum()
        {
            //Arrange
            var expected = 3;

            //Act
            var actual = _calculator.Add("1\n2");

            //Assert
            Assert.AreEqual(expected,actual);

        }
    }
}
